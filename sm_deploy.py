import sagemaker
import boto3
from sagemaker.pytorch import PyTorch
from sagemaker.pytorch import PyTorchModel
import argparse
parser = argparse.ArgumentParser()
sagemaker_session = sagemaker.Session(boto3.session.Session())


parser.add_argument('--role', type=str)
parser.add_argument('--bucket', type=str)
parser.add_argument('--modelname', type=str)
args = parser.parse_args()
print(args.role)
# Put the right role and input data
#role = "arn:aws:iam::869082236477:role/service-role/AmazonSageMaker-ExecutionRole-20201125T135948"

role = args.role
bucket_folder = args.bucket
bucket = bucket_folder.rsplit('/', 1)[0]
folder = bucket_folder.rsplit('/', 1)[-1]
modelname = args.modelname

prefix_ex = folder + '/' + modelname
s3 = boto3.resource('s3')
my_bucket = s3.Bucket(bucket)
for obj in my_bucket.objects.filter(Prefix=prefix_ex):
    Model =  obj.key

Com_Model = 's3://' + bucket + '/' + Model
print(Com_Model)

trainedmodel = sagemaker.model.Model(
    model_data= Com_Model, 
    image_uri = '811284229777.dkr.ecr.us-east-1.amazonaws.com/image-classification:latest',
    role=role)  # your role here; could be different name

#comment = Comment()
#values = comment.get_comment('model_data=')
#if values is None or len(values) == 0:
 #   comment.add_comment('Deploy Fail: no model data. Did you train?')
  #  exit(-1)

#print("Data:", values[-1])

#model = PyTorchModel(model_data=values[-1],
 #                    role=role,
  #                   framework_version='1.5.0',
   #                  entry_point='mnist.py',
    #                 source_dir='code')


#comment.add_comment('Deploying with data ' + values[-1])
try:
    predictor = trainedmodel.deploy(initial_instance_count=1, instance_type='ml.c4.xlarge',endpoint_name=modelname)
    print('end_point=' + predictor.endpoint)
except Exception as e:
    print('Deploy Fail:' + str(e))
